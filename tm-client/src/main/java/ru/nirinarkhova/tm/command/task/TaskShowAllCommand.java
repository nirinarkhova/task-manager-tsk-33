package ru.nirinarkhova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.endpoint.Session;
import ru.nirinarkhova.tm.endpoint.Task;
import ru.nirinarkhova.tm.exception.entity.ObjectNotFoundException;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

import static org.reflections.util.Utils.isEmpty;

public class TaskShowAllCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks, sort them.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[TASK LIST]");
        @Nullable List<Task> tasks;
        tasks = endpointLocator.getTaskEndpoint().findAllTask(session);
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task.getName() + " - " + task.getDescription());
            index++;
        }

    }

}
