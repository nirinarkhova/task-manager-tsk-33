package ru.nirinarkhova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.endpoint.Project;
import ru.nirinarkhova.tm.endpoint.Session;
import ru.nirinarkhova.tm.exception.entity.ObjectNotFoundException;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.*;

import static org.reflections.util.Utils.isEmpty;

public class ProjectShowAllCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[PROJECT LIST]");
        @Nullable List<Project> projects = endpointLocator.getProjectEndpoint().findProjectAll(session);
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + project.getName() + " - " + project.getDescription());
            index++;
        }
    }

}
