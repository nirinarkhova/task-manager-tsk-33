package ru.nirinarkhova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.endpoint.Session;
import ru.nirinarkhova.tm.endpoint.Task;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskByIndexRemoveCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete a task by index.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Task task = endpointLocator.getTaskEndpoint().removeTaskByIndex(session, index);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}
