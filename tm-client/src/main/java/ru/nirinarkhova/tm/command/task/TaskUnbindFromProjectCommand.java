package ru.nirinarkhova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.endpoint.Session;
import ru.nirinarkhova.tm.endpoint.Task;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-unbind-from-project";
    }

    @NotNull
    @Override
    public String description() {
        return "Unbind task from project.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[UNBIND TASK BY PROJECT]");
        System.out.println("[ENTER TASK ID:]");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final Optional<Task> task = endpointLocator.getProjectTaskEndpoint().unbindTaskFromProject(session, taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}
