package ru.nirinarkhova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.endpoint.Project;
import ru.nirinarkhova.tm.endpoint.Session;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectByIndexShowCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-view-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Find a project by index.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = endpointLocator.getProjectEndpoint().findProjectByIndex(session, index);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

}
