package ru.nirinarkhova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.endpoint.Project;
import ru.nirinarkhova.tm.endpoint.Session;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectByIdFinishCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Change project status to Complete by project id.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[FINISH PROJECT]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        final Project project = endpointLocator.getProjectEndpoint().finishByIdProject(session, id);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

}
