package ru.nirinarkhova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.endpoint.Project;
import ru.nirinarkhova.tm.endpoint.Session;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectByIndexUpdateCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-update-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Update a project by index.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Project project = endpointLocator.getProjectEndpoint().findProjectByIndex(session, index);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Optional<Project> projectUpdate = endpointLocator.getProjectEndpoint().updateByIndex(session, index, name, description);
        Optional.ofNullable(projectUpdate).orElseThrow(ProjectNotFoundException::new);
    }

}
