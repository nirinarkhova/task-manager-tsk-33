package ru.nirinarkhova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.endpoint.Session;
import ru.nirinarkhova.tm.endpoint.Task;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskByNameRemoveCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete a task by name.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final Task task = endpointLocator.getTaskEndpoint().removeTaskByName(session, name);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}
