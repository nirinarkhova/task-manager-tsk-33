package ru.nirinarkhova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Optional;

public class CommandsListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = endpointLocator.getCommandService().getCommands();
        for (@NotNull final AbstractCommand command : commands) {
            @NotNull final String name = command.name();
            if (!Optional.ofNullable(name).isPresent()) continue;
            System.out.println(name);
        }
    }

}
