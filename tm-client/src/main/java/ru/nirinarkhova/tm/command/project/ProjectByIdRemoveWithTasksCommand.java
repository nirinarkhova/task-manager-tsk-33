package ru.nirinarkhova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.endpoint.Project;
import ru.nirinarkhova.tm.endpoint.Session;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectByIdRemoveWithTasksCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-id-with-tasks";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete project with all its tasks.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[ENTER ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final Project project = endpointLocator.getProjectTaskEndpoint().removeProjectWithTasksById(session, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

}
