package ru.nirinarkhova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.endpoint.Project;
import ru.nirinarkhova.tm.endpoint.Session;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectByNameShowCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-view-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Show a project by name.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final Project project = endpointLocator.getProjectEndpoint().findProjectByName(session, name);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

}
