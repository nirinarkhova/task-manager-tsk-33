package ru.nirinarkhova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.endpoint.Session;
import ru.nirinarkhova.tm.endpoint.Task;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskByIndexUpdateCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Update a task by index.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Task task = endpointLocator.getTaskEndpoint().findTaskByIndex(session, index);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Task taskUpdate = endpointLocator.getTaskEndpoint().updateTaskByIndex(session, index, name, description);
        Optional.ofNullable(taskUpdate).orElseThrow(TaskNotFoundException::new);
    }

}
