package ru.nirinarkhova.tm.exception.authorization;

import ru.nirinarkhova.tm.exception.AbstractException;

public class EmailExistException extends AbstractException {

    public EmailExistException() {
        super("Error! Another user is already using this email. Please try again.");
    }

}
