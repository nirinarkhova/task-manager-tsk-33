package ru.nirinarkhova.tm.exception.empty;

import ru.nirinarkhova.tm.exception.AbstractException;

public class EmptyPasswordHashException extends AbstractException {

    public EmptyPasswordHashException() {
        super("Error! Password hash is empty.");
    }

}
