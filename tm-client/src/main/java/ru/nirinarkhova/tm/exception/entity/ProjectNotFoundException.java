package ru.nirinarkhova.tm.exception.entity;

import ru.nirinarkhova.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found.");
    }

}
