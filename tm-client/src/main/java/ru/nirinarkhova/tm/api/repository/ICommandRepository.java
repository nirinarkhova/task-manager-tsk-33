package ru.nirinarkhova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull Collection<AbstractCommand> getArgsCommands();

    @NotNull
    Collection<String> getCommandArgs();

    @NotNull
    Collection<String> getCommandNames();

    @Nullable
    AbstractCommand getCommandByArg(String arg);

    @Nullable
    AbstractCommand getCommandByName(String name);

    void add(@Nullable AbstractCommand command);

}
