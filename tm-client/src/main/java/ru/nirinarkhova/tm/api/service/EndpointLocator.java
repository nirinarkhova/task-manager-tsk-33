package ru.nirinarkhova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.nirinarkhova.tm.endpoint.*;
import ru.nirinarkhova.tm.service.CommandService;

public interface EndpointLocator {

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    CommandService getCommandService();

    @NotNull
    Session getSession();

    void setSession(Session session);

}
