package ru.nirinarkhova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.nirinarkhova.tm.api.other.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getDeveloperName();

}
