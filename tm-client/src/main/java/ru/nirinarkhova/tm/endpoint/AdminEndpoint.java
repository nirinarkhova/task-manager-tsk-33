package ru.nirinarkhova.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-07-12T10:57:29.080+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", name = "AdminEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AdminEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.nirinarkhova.ru/AdminEndpoint/clearTaskRequest", output = "http://endpoint.tm.nirinarkhova.ru/AdminEndpoint/clearTaskResponse")
    @RequestWrapper(localName = "clearTask", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.ClearTask")
    @ResponseWrapper(localName = "clearTaskResponse", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.ClearTaskResponse")
    public void clearTask(
        @WebParam(name = "session", targetNamespace = "")
        ru.nirinarkhova.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.nirinarkhova.ru/AdminEndpoint/projectClearRequest", output = "http://endpoint.tm.nirinarkhova.ru/AdminEndpoint/projectClearResponse")
    @RequestWrapper(localName = "projectClear", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.ProjectClear")
    @ResponseWrapper(localName = "projectClearResponse", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.ProjectClearResponse")
    public void projectClear(
        @WebParam(name = "session", targetNamespace = "")
        ru.nirinarkhova.tm.endpoint.Session session
    );
}
