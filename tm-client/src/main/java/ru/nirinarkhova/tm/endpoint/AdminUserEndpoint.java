package ru.nirinarkhova.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-07-12T10:57:29.372+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", name = "AdminUserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AdminUserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/unlockUserByLoginRequest", output = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/unlockUserByLoginResponse")
    @RequestWrapper(localName = "unlockUserByLogin", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.UnlockUserByLogin")
    @ResponseWrapper(localName = "unlockUserByLoginResponse", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.UnlockUserByLoginResponse")
    public void unlockUserByLogin(
        @WebParam(name = "session", targetNamespace = "")
        ru.nirinarkhova.tm.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/createUserWithRoleRequest", output = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/createUserWithRoleResponse")
    @RequestWrapper(localName = "createUserWithRole", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.CreateUserWithRole")
    @ResponseWrapper(localName = "createUserWithRoleResponse", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.CreateUserWithRoleResponse")
    public void createUserWithRole(
        @WebParam(name = "session", targetNamespace = "")
        ru.nirinarkhova.tm.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password,
        @WebParam(name = "role", targetNamespace = "")
        ru.nirinarkhova.tm.endpoint.Role role
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/removeUserRequest", output = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/removeUserResponse")
    @RequestWrapper(localName = "removeUser", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.RemoveUser")
    @ResponseWrapper(localName = "removeUserResponse", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.RemoveUserResponse")
    public void removeUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.nirinarkhova.tm.endpoint.Session session,
        @WebParam(name = "user", targetNamespace = "")
        ru.nirinarkhova.tm.endpoint.User user
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/findAllUserRequest", output = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/findAllUserResponse")
    @RequestWrapper(localName = "findAllUser", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.FindAllUser")
    @ResponseWrapper(localName = "findAllUserResponse", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.FindAllUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.nirinarkhova.tm.endpoint.User> findAllUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.nirinarkhova.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/clearUserRequest", output = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/clearUserResponse")
    @RequestWrapper(localName = "clearUser", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.ClearUser")
    @ResponseWrapper(localName = "clearUserResponse", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.ClearUserResponse")
    public void clearUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.nirinarkhova.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/removeUserByIdRequest", output = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/removeUserByIdResponse")
    @RequestWrapper(localName = "removeUserById", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.RemoveUserById")
    @ResponseWrapper(localName = "removeUserByIdResponse", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.RemoveUserByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.nirinarkhova.tm.endpoint.User removeUserById(
        @WebParam(name = "session", targetNamespace = "")
        ru.nirinarkhova.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/lockUserByLoginRequest", output = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/lockUserByLoginResponse")
    @RequestWrapper(localName = "lockUserByLogin", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.LockUserByLogin")
    @ResponseWrapper(localName = "lockUserByLoginResponse", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.LockUserByLoginResponse")
    public void lockUserByLogin(
        @WebParam(name = "session", targetNamespace = "")
        ru.nirinarkhova.tm.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/removeByLoginRequest", output = "http://endpoint.tm.nirinarkhova.ru/AdminUserEndpoint/removeByLoginResponse")
    @RequestWrapper(localName = "removeByLogin", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.RemoveByLogin")
    @ResponseWrapper(localName = "removeByLoginResponse", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.RemoveByLoginResponse")
    public void removeByLogin(
        @WebParam(name = "session", targetNamespace = "")
        ru.nirinarkhova.tm.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );
}
