package ru.nirinarkhova.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-07-12T10:57:30+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", name = "SessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface SessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.nirinarkhova.ru/SessionEndpoint/openSessionRequest", output = "http://endpoint.tm.nirinarkhova.ru/SessionEndpoint/openSessionResponse")
    @RequestWrapper(localName = "openSession", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.OpenSession")
    @ResponseWrapper(localName = "openSessionResponse", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.OpenSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.nirinarkhova.tm.endpoint.Session openSession(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.nirinarkhova.ru/SessionEndpoint/listSessionRequest", output = "http://endpoint.tm.nirinarkhova.ru/SessionEndpoint/listSessionResponse")
    @RequestWrapper(localName = "listSession", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.ListSession")
    @ResponseWrapper(localName = "listSessionResponse", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.ListSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.nirinarkhova.tm.endpoint.Session> listSession();

    @WebMethod
    @Action(input = "http://endpoint.tm.nirinarkhova.ru/SessionEndpoint/closeSessionRequest", output = "http://endpoint.tm.nirinarkhova.ru/SessionEndpoint/closeSessionResponse")
    @RequestWrapper(localName = "closeSession", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.CloseSession")
    @ResponseWrapper(localName = "closeSessionResponse", targetNamespace = "http://endpoint.tm.nirinarkhova.ru/", className = "ru.nirinarkhova.tm.endpoint.CloseSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.nirinarkhova.tm.endpoint.Session closeSession(
        @WebParam(name = "session", targetNamespace = "")
        ru.nirinarkhova.tm.endpoint.Session session
    );
}
