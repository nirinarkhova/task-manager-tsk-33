package ru.nirinarkhova.tm;

import ru.nirinarkhova.tm.bootstrap.Bootstrap;
import ru.nirinarkhova.tm.util.SystemUtil;

public class Application {

    public static void main(String[] args) throws Exception {
        System.out.println(SystemUtil.getPID());
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}