package ru.nirinarkhova.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.nirinarkhova.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(@NotNull final String command) {
        super("Error! Unknown ``" + command + "`` command.");
    }

}
