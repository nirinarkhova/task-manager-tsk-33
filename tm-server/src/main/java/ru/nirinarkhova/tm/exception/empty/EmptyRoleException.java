package ru.nirinarkhova.tm.exception.empty;

import ru.nirinarkhova.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error! Role is empty.");
    }

}
