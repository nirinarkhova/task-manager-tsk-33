package ru.nirinarkhova.tm.exception.authorization;

import ru.nirinarkhova.tm.exception.AbstractException;

public class LoginExistException extends AbstractException {

    public LoginExistException() {
        super("Error! Login already exists. Please try again.");
    }

}
