package ru.nirinarkhova.tm.exception.authorization;

import ru.nirinarkhova.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied.");
    }

}
