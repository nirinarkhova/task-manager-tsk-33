package ru.nirinarkhova.tm.exception.entity;

import ru.nirinarkhova.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found.");
    }

}
