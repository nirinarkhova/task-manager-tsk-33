package ru.nirinarkhova.tm.api.repository;

import ru.nirinarkhova.tm.api.IRepository;
import ru.nirinarkhova.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {
}
