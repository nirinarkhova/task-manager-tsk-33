package ru.nirinarkhova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.api.IBusinessService;
import ru.nirinarkhova.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    @NotNull
    Project add(@Nullable String userId, @Nullable String name, @Nullable String description);

}
