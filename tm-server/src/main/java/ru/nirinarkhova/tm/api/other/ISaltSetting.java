package ru.nirinarkhova.tm.api.other;

import org.jetbrains.annotations.NotNull;

public interface ISaltSetting {

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getSignSecret();

    @NotNull
    Integer getSignIteration();

}
