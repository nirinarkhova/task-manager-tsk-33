package ru.nirinarkhova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.api.IService;
import ru.nirinarkhova.tm.enumerated.Role;
import ru.nirinarkhova.tm.model.Session;

public interface ISessionService extends IService<Session> {

    @Nullable
    Session open(String login, String password);

    void validate(@Nullable Session session);

    @Nullable Session close(@Nullable Session session);

    void validateAdmin(@Nullable Session session, @Nullable Role role);

}
