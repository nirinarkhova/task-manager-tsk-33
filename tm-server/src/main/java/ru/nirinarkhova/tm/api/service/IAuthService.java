package ru.nirinarkhova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.enumerated.Role;
import ru.nirinarkhova.tm.model.User;

import java.util.Optional;

public interface IAuthService {

    @NotNull
    Optional<User> getUser();

    @NotNull
    String getUserId();

    void checkRoles(Role... roles);

    boolean isAuth();

    void logout();

    void login(@Nullable String login, @Nullable String password);

    void register(@Nullable String login, @Nullable String password, @Nullable String email);

}
