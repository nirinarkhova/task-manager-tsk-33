package ru.nirinarkhova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.nirinarkhova.tm.api.IBusinessRepository;
import ru.nirinarkhova.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IBusinessRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    Optional<Task> bindTaskByProject(@NotNull String userId, @NotNull String taskId, @NotNull String projectId);

    @NotNull
    Optional<Task> unbindTaskFromProject(@NotNull String userId, @NotNull String projectId);

}
