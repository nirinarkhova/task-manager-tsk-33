package ru.nirinarkhova.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    @NotNull
    List<E> findAll(@Nullable String userId);

    @NotNull
    List<E> findAll(@Nullable String userId, @NotNull Comparator<E> comparator);

    @NotNull
    E add(@NotNull String userId, @NotNull E entity);

    @NotNull
    Optional<E> findById(@Nullable String userId, @NotNull String id);

    @NotNull
    Optional<E> findByIndex(@Nullable String userId, @NotNull Integer index);

    @NotNull
    Optional<E> findByName(@Nullable String userId, @NotNull String name);

    void clear(@NotNull String userId);

    E removeById(@Nullable String userId, @NotNull String id);

    E removeByIndex(@Nullable String userId, @NotNull Integer index);

    E removeByName(@Nullable String userId, @NotNull String name);

    void remove(@Nullable String userId, @NotNull E entity);

}
