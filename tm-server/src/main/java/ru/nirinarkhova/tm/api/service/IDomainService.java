package ru.nirinarkhova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.dto.Domain;

public interface IDomainService {

    @NotNull
    Domain getDomain();

    void setDomain(@Nullable Domain domain);

    void loadBackup();

    void saveBackup();

    void loadBase64();

    void saveBase64();

    void loadBin();

    void saveBin();

    void loadJsonFasterXML();

    void loadJsonJaxb();

    void saveJsonFasterXML();

    void saveJsonJaxb();

    void loadXMLFasterXML();

    void loadXMLJaxb();

    void saveXMLFasterXML();

    void saveXMLJaxb();

    void loadYAMLFasterXML();

    void saveYAMLFasterXML();

}
