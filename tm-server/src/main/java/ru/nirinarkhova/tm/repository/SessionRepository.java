package ru.nirinarkhova.tm.repository;

import ru.nirinarkhova.tm.api.repository.ISessionRepository;
import ru.nirinarkhova.tm.model.Session;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
}
