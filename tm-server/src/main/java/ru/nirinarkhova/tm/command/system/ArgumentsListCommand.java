package ru.nirinarkhova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractCommand;

public class ArgumentsListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        /*@NotNull final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (@NotNull final AbstractCommand argument : arguments) {
            @NotNull final String arg = argument.arg();
            if (!Optional.ofNullable(arg).isPresent()) continue;
            System.out.println(arg);
        }

         */
    }

}
