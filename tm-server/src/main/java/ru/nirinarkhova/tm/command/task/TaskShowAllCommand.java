package ru.nirinarkhova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.api.service.ITaskService;
import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.enumerated.Sort;
import ru.nirinarkhova.tm.exception.entity.ObjectNotFoundException;
import ru.nirinarkhova.tm.model.Task;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

import static org.reflections.util.Utils.isEmpty;

public class TaskShowAllCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks, sort them.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        if (isEmpty(sort)) tasks = taskService.findAll();
        else tasks = taskService.findAll(userId, sort);
        if (tasks.isEmpty()) {
            System.out.println("[NO PROJECTS]");
            return;
        }
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println();
    }

}
