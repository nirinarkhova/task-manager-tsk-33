package ru.nirinarkhova.tm.endpoint;

import ru.nirinarkhova.tm.api.service.ServiceLocator;

public abstract class AbstractEndpoint {

    final ServiceLocator serviceLocator;

    public AbstractEndpoint(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
