package ru.nirinarkhova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.api.service.ServiceLocator;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.model.Session;
import ru.nirinarkhova.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectTaskEndpoint extends AbstractEndpoint {

    public ProjectTaskEndpoint() {
        super(null);
    }

    public ProjectTaskEndpoint(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }


    @NotNull
    @WebMethod
    public List<Task> findAllTaskByProjectId(
            @Nullable @WebParam(name = "userId") final String userId,
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        return serviceLocator.getProjectTaskService().findAllTaskByProjectId(userId, projectId);
    }

    @WebMethod
    public @Nullable Task bindTaskToProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "taskId") final String taskId,
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().bindTaskByProject(session.getUserId(), taskId, projectId).orElse(null);
    }

    @WebMethod
    public @Nullable Task unbindTaskFromProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "taskId") final String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().unbindTaskFromProject(session.getUserId(), taskId).orElse(null);
    }

    @WebMethod
    public @Nullable Project removeProjectWithTasksById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeProjectById(session.getUserId(), projectId);
    }

}
