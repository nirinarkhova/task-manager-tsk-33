package ru.nirinarkhova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.nirinarkhova.tm.api.service.ServiceLocator;
import ru.nirinarkhova.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class AdminEndpoint extends AbstractEndpoint {

    public AdminEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void clearTask(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear();
    }

    @WebMethod
    public void projectClear(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear();
    }

}
